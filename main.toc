\select@language {russian}
\contentsline {chapter}{Оглавление}{2}{dummy.1}
\contentsline {chapter}{\chapternumberline {1}Введение}{3}{chapter.3}
\contentsline {chapter}{\chapternumberline {2}Задача устранения размытости изображений}{5}{chapter.4}
\contentsline {section}{\numberline {2.1}Общая постановка задачи}{5}{section.5}
\contentsline {section}{\numberline {2.2}Обзор существующих методов решения}{5}{section.7}
\contentsline {section}{\numberline {2.3}Классические методы}{5}{section.12}
\contentsline {section}{\numberline {2.4}Методы, использующие априорную информацию}{6}{section.16}
\contentsline {section}{\numberline {2.5}Методы, явно моделирующие нелинейность}{7}{section.17}
\contentsline {section}{\numberline {2.6}Нейросетевые методы}{8}{section.20}
\contentsline {chapter}{\chapternumberline {3}Модель конкурирующих сетей}{11}{chapter.23}
\contentsline {section}{\numberline {3.1}Сверточные нейронные сети}{11}{section.24}
\contentsline {section}{\numberline {3.2}Конкурирующие сети}{12}{section.29}
\contentsline {chapter}{\chapternumberline {4}Исследование применимости конкурирующих сетей для решения задачи устранения размытости изображений}{15}{chapter.35}
\contentsline {section}{\numberline {4.1}Метрики качества}{15}{section.36}
\contentsline {section}{\numberline {4.2}Стандартная модель конкурирующих сетей}{16}{section.37}
\contentsline {section}{\numberline {4.3}Комбинирование стандартного и конкурирующего обучения}{18}{section.42}
\contentsline {section}{\numberline {4.4}Использование скрытых представлений, формируемых дискриминативной сетью в функции ошибки}{22}{section.54}
\contentsline {section}{\numberline {4.5}Минимизация функции ошибки, порождаемой глубинной нейронной сетью}{23}{section.59}
\contentsline {chapter}{\chapternumberline {5}Заключение}{27}{chapter.66}
\contentsline {chapter}{Литература}{28}{section*.68}
